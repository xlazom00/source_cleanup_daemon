#!/usr/bin/python2.7
# -*- coding: utf8 -*-

import time
import sqlobject
from sqlobject import *
import MySQLdb.converters
import sys, os
import re
from sqlobject.col import StringCol
import string


def _mysql_timestamp_converter(raw):
    """Convert a MySQL TIMESTAMP to a floating point number representing
    the seconds since the Un*x Epoch. It uses custom code the input seems
    to be the new (MySQL 4.1+) timestamp format, otherwise code from the
    MySQLdb module is used."""
    if raw[4] == '-':
        return time.mktime(time.strptime(raw, '%Y-%m-%d %H:%M:%S'))
    else:
        return MySQLdb.converters.mysql_timestamp_converter(raw)


conversions = MySQLdb.converters.conversions.copy()
conversions[MySQLdb.constants.FIELD_TYPE.TIMESTAMP] = _mysql_timestamp_converter

MySQLConnection = sqlobject.mysql.builder()
connection = MySQLConnection(host='localhost', user='streamer', password='streamer', db='streamer', conv=conversions,
                             use_unicode=True, charset="utf8", debug='false')
sqlhub.processConnection = connection

defaultSegmentDuration = 10 * 1000

# month
month = 2630000000
# week
week  = 604800000
# day
day =   86400000


###################################################
# erase time
eraseTime = 14 * day
###################################################
###################################################
# dataRoot = "/home/misko/device_pi2_live/"
dataRoot = "/media/live/"
###################################################

class Channel(SQLObject):
    vdrId = StringCol()
    name = StringCol()


class Event(SQLObject):
    vdrId = IntCol()
    startTime = BigIntCol()
    duration = IntCol()
    title = StringCol()
    shortText = StringCol()
    description = StringCol()
    channel = ForeignKey('Channel')


class FileSegment(SQLObject):
    fileId = BigIntCol()
    startTime = BigIntCol()
    duration = IntCol()
    channel = ForeignKey('Channel')
    firstLastIndex = DatabaseIndex('fileId', 'channel')

fstime = 0
dbtime = 0
selectdbtime = 0

def eventDuration(durationinsec):
    return durationinsec * 1000

def getOldestEvents(endTime):
    res = Event.select(Event.q.startTime <= endTime, orderBy=Event.q.startTime)
    return list(res[:10])


def get_oldest_filesegments(endTime):
    res = FileSegment.select(FileSegment.q.startTime <= endTime, orderBy=FileSegment.q.startTime)
    # res = FileSegment.select(FileSegment.q.startTime <= endTime)
    return list(res[:100])


def getFileSegments(startTime, endTime, channel):
    global selectdbtime
    a1 = time.time()
    res = FileSegment.select(AND(FileSegment.q.startTime >= (startTime - defaultSegmentDuration),
                                 FileSegment.q.startTime <= endTime,
                                 FileSegment.q.channel == channel))
    reslist = list(res)
    a2 = time.time()
    selectdbtime += (a2 - a1)

    return reslist

def delete_event(event):
    Event.delete(event.id)

def format_file_segment(fileSegment, channelid, quality):
    fileName = ("%010d.ts") % fileSegment.fileId
    return dataRoot + str(channelid) + "/" + quality + "/" + fileName

def delete_file_segment2(fileSegment, quality):
    # delete fileSegment from FS
    fullfilename = format_file_segment(fileSegment, fileSegment.channel.id, quality)
    try:
        global fstime
        a1 = time.time()

        # print fullfilename

        os.remove(fullfilename)
        a2 = time.time()
        fstime += (a2-a1)
        print fullfilename

    except:
        print "not found:" + fullfilename
        pass

    # and DB
    a1 = time.time()
    try:
	FileSegment.delete(fileSegment.id)
    except:
	pass
    a2 = time.time()
    global dbtime
    dbtime += a2-a1


def delete_file_segment(fileSegment, channelid, quality):
    # delete fileSegment from FS
    fullfilename = format_file_segment(fileSegment, channelid, quality)
    try:
        global fstime
        a1 = time.time()

        os.remove(fullfilename)
        a2 = time.time()
        fstime += (a2-a1)
        # print fullfilename

    except:
        # print "not found:" + fullfilename
        pass
    # and DB
    # a1 = time.time()
    # FileSegment.delete(fileSegment.id)
    # a2 = time.time()
    # global dbtime
    # dbtime += a2-a1

def delete_file_segments(fileSegmentList, channel, channelId):
    if len(fileSegmentList) == 0:
        return
    a1 = time.time()

    sortedSegmentList = sorted(fileSegmentList, key=lambda x: x.fileId, reverse=False)
    lowest = sortedSegmentList[0].fileId
    highest = sortedSegmentList[-1].fileId
    FileSegment.deleteMany(AND(FileSegment.q.channel == channel,
                               FileSegment.q.fileId >= lowest,
                               FileSegment.q.fileId <= highest))

    a2 = time.time()
    global dbtime
    dbtime += (a2-a1)


def print_time(dbtime):
    print(time.gmtime(dbtime/1000))

def print_time2(str, dbtime):
    print(str + time.gmtime(dbtime/1000).__str__())


eventDB = 0
channelDB = 0

# Event.dropTable()
# Channel.dropTable()


try:
    Channel.createTable()
except:
    pass

try:
    Event.createTable()
except:
    pass

try:
    FileSegment.createTable()
except:
    pass

# exit

# in ms
currentTime = int(round(time.time() * 1000))

endTime = currentTime - eraseTime

print_time2("current time:", currentTime)
print_time2("end time:", endTime)
print(endTime)
cleanupstop = False

while True:
    if cleanupstop:
        break
    aa = getOldestEvents(endTime)
    if len(aa) == 0:
        break

    for event in aa:
        channel = event.channel
        channelId = channel.id
        fileSegmentList = getFileSegments(event.startTime, event.startTime + eventDuration(event.duration), event.channel)


        if len(fileSegmentList) == 0:
            print "event don't contain any FileSegments"
            pass
        else:
            print
            print "[{2}]: {0} {3} {1}".format(event.title, time.gmtime(event.startTime/1000).__str__(), channel.name, event.duration)

            print "removing FileSegments in event"
            print "cleaning {0} segments".format(len(fileSegmentList))
            # cleanupstop = True
            # break

        delete_event(event)

        # FS clean
        for fileSegment in fileSegmentList:
            delete_file_segment(fileSegment, channel.id, "100")
	    delete_file_segment(fileSegment, channel.id, "030")
	    delete_file_segment(fileSegment, channel.id, "005")

        # db clean
        delete_file_segments(fileSegmentList, channel, channelId)

        if len(fileSegmentList) > 0:
            print fstime
            print dbtime
            print selectdbtime


# exit()

# clean all filesegments
endTime -= day

print_time2("end time:", endTime)
print(endTime)

while True:
    if cleanupstop:
        break
    fileSegmentList = get_oldest_filesegments(endTime)
    print fileSegmentList

    if len(fileSegmentList) == 0:
        break

    print "cleaning {0} segments".format(len(fileSegmentList))

    # FS and DB clean
    for fileSegment in fileSegmentList:
            delete_file_segment2(fileSegment, "100")
	    delete_file_segment2(fileSegment, "030")
	    delete_file_segment2(fileSegment, "005")


